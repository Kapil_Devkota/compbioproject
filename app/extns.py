from functools import reduce

# first elem of list is use for indexing
EXT_DCT = {}
def load(l):
	for e in l:
		EXT_DCT[e] = l[0]

GENBANK = ['genbank', 'gb', 'gbk']
FASTA = ['fasta', 'fas', 'fa', 'seq', 'fsa', 'frn']
MISC = ['txt', 'out']

ALL = [GENBANK, FASTA, MISC]
[load(l) for l in ALL]

ALLOWED_EXTENSIONS = MISC+FASTA+GENBANK
EXTENSION_COMPLAIN = reduce(lambda x,y: x+' '+y, ['.'+e for e in ALLOWED_EXTENSIONS], "accepted file formats: ")
