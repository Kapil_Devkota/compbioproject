import sys
import os
sys.path.append('..')
from functools import reduce, partial

from Source import palindrome, file_parse
from flask import render_template, redirect, url_for

from app import app, db

from app.forms import ParamsForm
from app.models import Sequence, Palindrome

from werkzeug.utils import secure_filename

from flask import request, Response
from werkzeug.datastructures import CombinedMultiDict

def gen_cont(s, p):
    pdb = Palindrome(source=s)
    pdb.set_dict_from(p)
    db.session.add(pdb)
    db.session.commit()
    return str(p)

def stream_template(template_name, **context):
    app.update_template_context(context)
    t = app.jinja_env.get_template(template_name)
    rv = t.stream(context)
    return rv

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html', title='Home')

@app.route('/params', methods=['GET', 'POST'])
def params():

    def param_name(n, a, b, c, d, e, f):
        return reduce(lambda x,y: x+"_"+y, list(map(str, [a, b, c, d, e, f])), n)

    form = ParamsForm(CombinedMultiDict((request.files, request.form)),
                      margin=1, 
                      min_arm_errors=20, 
                      min_gap_errors=2, 
                      min_arm_length=50, 
                      min_extension=40, 
                      extension_additional_errors=20)

    if form.validate_on_submit():
        f = form.file.data
        fname = secure_filename(f.filename)
        f.save(os.path.join(app.instance_path, 'files', fname))

        nms_sqs = file_parse.parse(fname)

        margin=form.margin.data
        min_arm_errors=form.min_arm_errors.data
        min_gap_errors=form.min_gap_errors.data
        min_arm_length=form.min_arm_length.data
        min_extension=form.min_extension.data
        extension_additional_errors=form.extension_additional_errors.data   

        def stream_pals():
          for nm, raw_seq in nms_sqs:

              pnm = param_name(nm, 
                               margin, 
                               min_gap_errors, 
                               min_arm_errors, 
                               min_arm_length, 
                               min_extension, 
                               extension_additional_errors)

              xs = Sequence.query.filter_by(name=pnm)
              fname = pnm + ".out.txt"
              if len(list(xs)) == 0:
                  seq = Sequence(name=pnm)
                  db.session.add(seq)
                  db.session.commit()
                  s = palindrome.String(raw_seq, 
                                        margin=margin,
                                        min_arm_errors=min_arm_errors,
                                        min_gap_errors=min_gap_errors,
                                        min_arm_length=min_arm_length,
                                        min_extension=min_extension,
                                        extension_additional_errors=extension_additional_errors)
                  yield (pnm, s.find_all_palindromes(partial(gen_cont, seq)), fname)
              else:
                  yield (pnm, None, fname)
        
        return Response(stream_template('results.html', title="Results", data=stream_pals()))
    else:
        return render_template('params.html', title="Customized Palindrome Detection", form=form)


@app.route('/download/<sname>', methods=['GET', 'POST'])
def download(sname):
    def convert_to_pal(i, l, m, r, e):
        return str(i) + " " + str(l) + ((lambda x: " "+str(x) if x else "")(m)) + " " + str(r) + " " + str(e) + "\n"

    ps = Sequence.query.filter_by(name=str(sname)).first().palindromes
    temp = list(map(lambda x: x.make_with(convert_to_pal), list(ps)))
    fdata = list(reduce(lambda x,y: x+y, temp, ""))
    fdata = ["[position] [palindrome] [errors]\n"] + ["[0-9]* [a|t|c|g]* ([a|t|c|g] ){0,1}[a|t|c|g]* [0-9]*\n"] + fdata 
    return Response(fdata, mimetype='text/plain', headers={'Content-Disposition':("attachment;filename="+str(sname)+'.out.txt')})

@app.route('/panic!', methods=['GET', 'POST'])
def panic():
    sqs = Sequence.query.all()

    for s in sqs:
        ps = s.palindromes
        for p in ps:
            db.session.delete(p)
        db.session.delete(s)
    db.session.commit()

    return redirect(url_for('downloads'))

@app.route('/downloads', methods=['GET', 'POST'])
def downloads():
    sqs = Sequence.query.all()
    data = list(map(lambda x: (x, len(list(x.palindromes))), list(sqs)))
    return render_template('downloads.html', title='Download Sequences', sqs=data)

