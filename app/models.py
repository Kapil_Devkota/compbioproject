from app import db

class Sequence(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), index=True, unique=True)
    palindromes = db.relationship('Palindrome', backref='source', lazy='dynamic')

    def __repr__(self):
        return "Sequence: " + self.name

class Palindrome(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pal_dict = db.Column(db.PickleType, nullable=False)
    sequence_id = db.Column(db.Integer, db.ForeignKey('sequence.id'))

    def __repr__(self):
        return 'Palindrome: '+str(self.pal_dict)

    def set_dict_from(self, p):
        d = {'mid':p.mid, 'left_arm':p.left_arm, 'right_arm':p.right_arm, 'mid_pt':p.mid_pt, 'errors':p.errors}
        self.pal_dict = d

    def make_with(self, f):
        return f(self.pal_dict['mid_pt'],
                 self.pal_dict['left_arm'],
                 self.pal_dict['mid'],
                 self.pal_dict['right_arm'],
                 self.pal_dict['errors'])

class Test2(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    object = db.Column(db.PickleType, nullable=False)
