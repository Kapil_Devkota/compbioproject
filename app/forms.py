from functools import reduce

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from wtforms import StringField, SubmitField, TextAreaField, IntegerField
from wtforms.validators import InputRequired, ValidationError
from werkzeug.utils import secure_filename

from app.extns import ALLOWED_EXTENSIONS, EXTENSION_COMPLAIN

class ParamsForm(FlaskForm):
	file = FileField('Select sequence file', validators=[FileRequired(), FileAllowed(ALLOWED_EXTENSIONS, EXTENSION_COMPLAIN)])
	margin = IntegerField('Margin', validators=[InputRequired()])
	min_arm_errors = IntegerField('Maximum allowed irregularities in arm', validators=[InputRequired()])
	min_gap_errors = IntegerField('Maximum allowed irregularities in gap', validators=[InputRequired()])
	min_arm_length = IntegerField('Minimum palindrome arm length', validators=[InputRequired()])
	min_extension = IntegerField('Minimum allowed extension length', validators=[InputRequired()])
	extension_additional_errors = IntegerField('Maximum additional allowed extension irregularities', validators=[InputRequired()])
	submit = SubmitField('Find palindromes')