def preprocess_genbank(filename):
	
	file = open(filename , "r")

	metadata = ""
	seq = ""
	is_metadata = True

	for line in file:
		line = line.rstrip("\r\n")
		
		if(is_metadata == True):

			metadata += line + "\n"
			if("ORIGIN" in line):	
				is_metadata = False
		else:			
			if("//" not in line):
				seq += line + "\n"


	return metadata , seq


def preprocess_genbank_seq(r_sequence):
	
	lines = r_sequence.split("\n")

	sequence = ""
	for line in lines:
		words = line.lstrip().split(" ")
		sequence += "".join(words[1 : ])

	sequence = sequence.rstrip("\n")
	return sequence.lower()


def preprocess_fasta(filename):
	
	file = open(filename , "r")
	
	sequence = ""	
		
	for line in file:
		
		if(line[0] != '>'):
			sequence += line.rstrip("\r\n")

	return sequence.lower()
