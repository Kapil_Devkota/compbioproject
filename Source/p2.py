import lev

class Palindrome:

    def __init__(self, palindrome, s):
        mid_pt, arm_len = palindrome[5], palindrome[2]
        self.left_arm = s[mid_pt-arm_len : mid_pt]
        self.right_arm = s[mid_pt+1 : mid_pt+arm_len+1]
        if palindrome[0]:
            self.mid = s[mid_pt]
        else:
            self.mid = None
            self.left_arm += s[mid_pt+1]
        self.mid_pt = mid_pt

    def __repr__(self):
        return str(self.mid_pt) + ' | ' + self.left_arm + ' ' + str((lambda x: x if x else '')(self.mid)) + ' ' + self.right_arm + ' ' + str(self.errors)

    def errors(self):
        return lev.iterative_levenshtein(self.left_arm, self.right_arm)


class String:

    def __init__(self, string, margin=1, min_arm_errors=20, min_gap_errors=2, min_arm_length= 50 , min_extension = 40 , extension_additional_errors = 20):
        self.string = string
        self.margin = margin
        self.min_arm_errors = min_arm_errors
        self.min_gap_errors = min_gap_errors
        self.min_arm_length = min_arm_length
        self.min_extension = min_extension
        self.str_len = len(string)
        self.extension_additional_errors = extension_additional_errors
        self.all_palindromes = []
        return
    
    def equal(self , char , char_):
        order = ord(char) + ord(char_)
        return (order == 202 or order == 213)

    def find_max_odd_palindrome(self , i , offset = 1 , prev_palindrome = None):

        _begin = 0
        _offset = offset
        _gerror = 0

        _extension = (offset > 1)

        _extension_possible = False

        _aerror = 0
        _consecutive_errors = 0
        _additional_errors = 0
        _min_arm_errors = self.min_arm_errors

        if(_extension == True):
            _min_arm_errors = self.extension_additional_errors + prev_palindrome[6]
 
        #print("Index[" + str(i) + "] , " + "Type[Odd]" + " Extension[" + str(_extension) + "]")

        while(True):

            if (i - _offset < 0):
                _offset = i
                break

            if (i + _offset > self.str_len - 1):
                _offset = self.str_len - i - 1
                break


            if(_aerror >= _min_arm_errors):
                break

            if (_offset < self.margin and _extension == False):

                if(not self.equal(string[i - _offset] , self.string[i + _offset])):
                    _gerror += 1

            else:
                #print(self.string[i - _offset] + " --- " + self.string[i + _offset])
                if(not self.equal(self.string[i - _offset] , self.string[i + _offset])):
                    #print("[-]")
                    _aerror += 1
                    _consecutive_errors += 1
            
                    if(_consecutive_errors > 4):
                        _offset -= _consecutive_errors
                        _aerror -= _consecutive_errors
                        _extension_possible = True
                        break

                else:
                    #print("[+]")
                    _consecutive_errors = 0
            _offset += 1
        
        #print("\n")

        _offset -= 1

        if(not _extension):
            _g_offset = self.margin 
            if(_gerror < self.min_gap_errors):
                _g_offset = int(_gerror * self.min_gap_errors / self.margin)
        
        else:
            _g_offset = offset

		        
        if(_extension and _offset - _g_offset < self.min_extension):
            #print("[[-]]\n")
            return None

        if(not _extension and _offset - _g_offset < self.min_arm_length):
            #print("[[-]]\n")
            return None

        if _extension:
            #print("[[+]]\n")
            return (True , prev_palindrome[1] , _offset , i , _aerror , prev_palindrome[5] , prev_palindrome[6] + 1 , _extension_possible)

        #print("[[+]]\n")
        return (True , _g_offset , _offset , i , _aerror , i , 0 , _extension_possible)


    def find_max_even_palindrome(self , i , offset = 1 , prev_palindrome = None):
    
        _begin = 0
        _offset = offset
        _gerror = 0
        _aerror = 0

        _extension = (offset > 1)

        _aerror = 0
        _consecutive_errors = 0
        _min_arm_errors = self.min_arm_errors
	
        _extension_possible = False

        if _extension:
            _min_arm_errors = self.extension_additional_errors + prev_palindrome[6]

        #print("Index[" + str(i) + "] , " + "Type[Even]" + " Extension[" + str(_extension) + "]")

        while(True):

            if(i - _offset + 1 < 0):
                _offset = i + 1
                break

            if(i + _offset > self.str_len - 1):
                _offset = self.str_len - 1 - i
                break 

            if(_aerror >= _min_arm_errors):
                break

            if(_offset < self.margin and _extension == False):
                if(not self.equal(self.string[i - _offset + 1] , self.string[i + _offset])):
                    _gerror += 1;

            else:

                #print(self.string[i - _offset + 1] + " --- " + self.string[i + _offset])
                if(not self.equal(self.string[i - _offset + 1] , self.string[i + _offset])):
                    #print("[-]")
                    _aerror += 1;
                    _consecutive_errors += 1
            
                    if(_consecutive_errors > 4):
                        _offset -= _consecutive_errors
                        _aerror -= _consecutive_errors
                        _extension_possible = True
                        break

                else:
                    #print("[+]")
                    _consecutive_errors = 0

            
            _offset += 1

        #print("\n")
        _offset -= 1
        
        if(not _extension):
            _g_offset = self.margin 
            if(_gerror < self.min_gap_errors):
                _g_offset = int(_gerror * self.min_gap_errors / self.margin)

        else:
            _g_offset = offset

 
        if(_extension and _offset - _g_offset < self.min_extension):
            #print("[[-]]\n")
            return None

        if(not _extension and _offset - _g_offset < self.min_arm_length):
            #print("[[-]]\n")
            return None


        if _extension:
            #print("[[+]]\n")
            return (False , prev_palindrome[1] , _offset , i , _aerror , prev_palindrome[5] , prev_palindrome[6] + 1 , _extension_possible)

        #print("[[+]]\n")
        return (False , _g_offset , _offset , i , _aerror , i , 0 , _extension_possible)


    def find_all_palindromes(self , start = 0 , end = -1):

        if(end < 0): 
            end = self.str_len - 1

        _i = start

        while(_i < end):
            #print("---------------Position [" + str(_i) + "]--------------------")
            if(len(self.all_palindromes) != 0):

                _prev_palindrome = self.all_palindromes[-1]
        
                if(_prev_palindrome[3] == _i - 1 and _prev_palindrome[7] == True):
                    if(_prev_palindrome[0] == True):
                        _p = self.find_max_even_palindrome(_i , offset = _prev_palindrome[2] , prev_palindrome = _prev_palindrome)
                    else:
                        _p = self.find_max_odd_palindrome(_i , offset = _prev_palindrome[2] , prev_palindrome = _prev_palindrome)
                    
                    if(_p != None):
                        del self.all_palindromes[-1]
                        self.all_palindromes.append(_p)
                        continue
            
            _p = self.find_max_odd_palindrome(_i)
            
            if(_p == None):  
                _p = self.find_max_even_palindrome(_i)
            
            if(_p != None):
                self.all_palindromes.append(_p)
            
            _i += 1                         


        return sorted(self.all_palindromes , key = lambda a : a[2] - a[1] , reverse = True)

    def palindrome_str(self, p):
        ip = p[5]
        a = p[2]
        if(p[0]):
            return "Index : " + str(ip) + " | " + self.string[ip - a : ip] + " " + self.string[ip] + " " + self.string[ip + 1 : ip + a + 1]
        else:
            return "Index : " + str(ip) + " | " + self.string[ip - a : ip + 1] + " " + self.string[ip + 1 : ip + a + 1]

    def print_all_palindromes(self, palindromes):
        xs = map(lambda x: self.palindrome_str(x), palindromes)
        for x in xs:
            print x
        return xs

    def make_palindrome(self, p):
        return Palindrome(p, self.string)

    def palindrome_list(self):
        return list(map(self.make_palindrome, self.all_palindromes))

def sequence_string():
    file = open("sequence.out" , "r")
    sequence = "" 
    for line in file:
        line = line.rstrip("\r\n")
        sequence += line
    return String(sequence)

def strings(ps, s):
    sp = String(s)
    return map(lambda x: sp.palindrome_str(x), ps)

def fromSeq(seq):
    s = String(seq.replace(' ','').replace('\n','').replace('\t',''))
    return (s.find_all_palindromes(), s.string)

def test():
    file = open("sequence.out" , "r")
    sequence = "" 
    for line in file:
        line = line.rstrip("\r\n")
        sequence += line

    string = String(sequence)

    palindromes = string.find_all_palindromes()

    string.print_all_palindromes(palindromes)

    if palindromes:
        return palindromes, string.string

        

