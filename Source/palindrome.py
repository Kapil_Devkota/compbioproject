def iterative_levenshtein(s, t):
    """                                                                                                                                                                                                     
        iterative_levenshtein(s, t) -> ldist                                                                                                                                                                
        ldist is the Levenshtein distance between the strings                                                                                                                                               
        s and t.                                                                                                                                                                                            
        For all i and j, dist[i,j] will contain the Levenshtein                                                                                                                                             
        distance between the first i characters of s and the                                                                                                                                                
        first j characters of t                                                                                                                                                                             
    """
    rows = len(s)+1
    cols = len(t)+1
    dist = [[0 for x in range(cols)] for x in range(rows)]
    # source prefixes can be transformed into empty strings                                                                                                                                                 
    # by deletions:                                                                                                                                                                                         
    for i in range(1, rows):
        dist[i][0] = i
    # target prefixes can be created from an empty source string                                                                                                                                            
    # by inserting the characters                                                                                                                                                                           
    for i in range(1, cols):
        dist[0][i] = i

    for col in range(1, cols):
        for row in range(1, rows):
            if s[row-1] == t[col-1]:
                cost = 0
            else:
                cost = 1
            dist[row][col] = min(dist[row-1][col] + 1,      # deletion                                                                                                                                      
                                 dist[row][col-1] + 1,      # insertion                                                                                                                                     
                                 dist[row-1][col-1] + cost) # substitution                                                                                                                                  

    return dist[row][col]

class Palindrome:

    def __init__(self, palindrome, s):
        mid_pt, arm_len = palindrome[5], palindrome[2]
        self.left_arm = s[mid_pt-arm_len : mid_pt]
        self.right_arm = s[mid_pt+1 : mid_pt+arm_len+1]
        self.mid = (lambda x: s[mid_pt] if x else None)(palindrome[0])
        self.mid_pt = mid_pt
        self.errors = iterative_levenshtein(self.left_arm, self.right_arm)

    def __repr__(self):
        return str(self.mid_pt) + ' ' + self.left_arm + str((lambda x: ' ' + x if x else '')(self.mid)) + ' ' + self.right_arm + ' ' + str(self.errors)


class String:

    def __init__(self, string, margin=1, min_arm_errors=20, min_gap_errors=2, min_arm_length= 50 , min_extension = 40 , extension_additional_errors = 20):
        self.string = string
        self.margin = margin
        self.min_arm_errors = min_arm_errors
        self.min_gap_errors = min_gap_errors
        self.min_arm_length = min_arm_length
        self.min_extension = min_extension
        self.str_len = len(string)
        self.extension_additional_errors = extension_additional_errors
        self.all_palindromes = []
        return
    
    def equal(self , char , char_):
        order = ord(char) + ord(char_)
        return (order == 202 or order == 213)

    def find_max_odd_palindrome(self , i , offset = 1 , prev_palindrome = None):

        _begin = 0
        _offset = offset
        _gerror = 0

        _extension = (offset > 1)

        _extension_possible = False

        _aerror = 0
        _consecutive_errors = 0
        _additional_errors = 0
        _min_arm_errors = self.min_arm_errors

        if(_extension == True):
            _min_arm_errors = self.extension_additional_errors + prev_palindrome[6]
 
        #print("Index[" + str(i) + "] , " + "Type[Odd]" + " Extension[" + str(_extension) + "]")

        while(True):

            if (i - _offset < 0):
                _offset = i
                break

            if (i + _offset > self.str_len - 1):
                _offset = self.str_len - i - 1
                break


            if(_aerror >= _min_arm_errors):
                break

            if (_offset < self.margin and _extension == False):

                if(not self.equal(self.string[i - _offset] , self.string[i + _offset])):
                    _gerror += 1

            else:
                #print(self.string[i - _offset] + " --- " + self.string[i + _offset])
                if(not self.equal(self.string[i - _offset] , self.string[i + _offset])):
                    #print("[-]")
                    _aerror += 1
                    _consecutive_errors += 1
            
                    if(_consecutive_errors > 4):
                        _offset -= _consecutive_errors
                        _aerror -= _consecutive_errors
                        _extension_possible = True
                        break

                else:
                    #print("[+]")
                    _consecutive_errors = 0
            _offset += 1
        
        #print("\n")

        _offset -= 1

        if(not _extension):
            _g_offset = self.margin 
            if(_gerror < self.min_gap_errors):
                _g_offset = int(_gerror * self.min_gap_errors / self.margin)
        
        else:
            _g_offset = offset

		        
        if(_extension and _offset - _g_offset < self.min_extension):
            #print("[[-]]\n")
            return None

        if(not _extension and _offset - _g_offset < self.min_arm_length):
            #print("[[-]]\n")
            return None

        if _extension:
            #print("[[+]]\n")
            return (True , prev_palindrome[1] , _offset , i , _aerror , prev_palindrome[5] , prev_palindrome[6] + 1 , _extension_possible)

        #print("[[+]]\n")
        return (True , _g_offset , _offset , i , _aerror , i , 0 , _extension_possible)


    def find_max_even_palindrome(self , i , offset = 1 , prev_palindrome = None):
    
        _begin = 0
        _offset = offset
        _gerror = 0
        _aerror = 0

        _extension = (offset > 1)

        _aerror = 0
        _consecutive_errors = 0
        _min_arm_errors = self.min_arm_errors
	
        _extension_possible = False

        if _extension:
            _min_arm_errors = self.extension_additional_errors + prev_palindrome[6]

        #print("Index[" + str(i) + "] , " + "Type[Even]" + " Extension[" + str(_extension) + "]")

        while(True):

            if(i - _offset + 1 < 0):
                _offset = i + 1
                break

            if(i + _offset > self.str_len - 1):
                _offset = self.str_len - 1 - i
                break 

            if(_aerror >= _min_arm_errors):
                break

            if(_offset < self.margin and _extension == False):
                if(not self.equal(self.string[i - _offset + 1] , self.string[i + _offset])):
                    _gerror += 1;

            else:

                #print(self.string[i - _offset + 1] + " --- " + self.string[i + _offset])
                if(not self.equal(self.string[i - _offset + 1] , self.string[i + _offset])):
                    #print("[-]")
                    _aerror += 1;
                    _consecutive_errors += 1
            
                    if(_consecutive_errors > 4):
                        _offset -= _consecutive_errors
                        _aerror -= _consecutive_errors
                        _extension_possible = True
                        break

                else:
                    #print("[+]")
                    _consecutive_errors = 0

            
            _offset += 1

        #print("\n")
        _offset -= 1
        
        if(not _extension):
            _g_offset = self.margin 
            if(_gerror < self.min_gap_errors):
                _g_offset = int(_gerror * self.min_gap_errors / self.margin)

        else:
            _g_offset = offset

 
        if(_extension and _offset - _g_offset < self.min_extension):
            #print("[[-]]\n")
            return None

        if(not _extension and _offset - _g_offset < self.min_arm_length):
            #print("[[-]]\n")
            return None


        if _extension:
            #print("[[+]]\n")
            return (False , prev_palindrome[1] , _offset , i , _aerror , prev_palindrome[5] , prev_palindrome[6] + 1 , _extension_possible)

        #print("[[+]]\n")
        return (False , _g_offset , _offset , i , _aerror , i , 0 , _extension_possible)


    def find_all_palindromes(self , gen_cont, start = 0 , end = -1):

        if(end < 0): 
            end = self.str_len - 1

        _i = start

        while(_i < end):
            #print("---------------Position [" + str(_i) + "]--------------------")
            if(len(self.all_palindromes) != 0):

                _prev_palindrome = self.all_palindromes[-1]
        
                if(_prev_palindrome[3] == _i - 1 and _prev_palindrome[7] == True):
                    if(_prev_palindrome[0] == True):
                        _p = self.find_max_even_palindrome(_i , offset = _prev_palindrome[2] , prev_palindrome = _prev_palindrome)

                    else:
                        _p = self.find_max_odd_palindrome(_i , offset = _prev_palindrome[2] , prev_palindrome = _prev_palindrome)
                    
                    if(_p != None):
                        del self.all_palindromes[-1]
                        self.all_palindromes.append(_p)
                        yield gen_cont(Palindrome(_p, self.string))
                        continue
            
            _p = self.find_max_odd_palindrome(_i)
            
            if(_p == None):  
                _p = self.find_max_even_palindrome(_i)
            
            if(_p != None):
                self.all_palindromes.append(_p)
                yield gen_cont(Palindrome(_p, self.string))
            
            _i += 1                         

        #self.all_palindromes = sorted(self.all_palindromes , key = lambda a : a[2] - a[1] , reverse = True)
        #return self.all_palindromes

class Palindromes:

    def __init__(self, s):
        self.string = s.string
        self.palindromes = list(map(lambda x: Palindrome(x, self.string), s.all_palindromes))

def pals_from_seq(seq, margin, min_arm_errors, min_gap_errors, min_arm_length,min_extension,extension_additional_errors):
    s = String(seq.replace(' ','').replace('\n','').replace('\t',''),
               margin=margin,
               min_arm_errors=min_arm_errors,
               min_gap_errors=min_gap_errors,
               min_arm_length=min_arm_length,
               min_extension=min_extension,
               extension_additional_errors=extension_additional_errors)

    s.find_all_palindromes()
    return Palindromes(s)        

