import sys
sys.path.append('..')

import functools

from Bio import SeqIO

from app import extns

def txt(fname):
    with open(fname, "r") as stream:
        data = stream.readlines() 
    temp = functools.reduce(lambda x,y: x+y, data, "")
    return [(fname, temp.replace(' ','').replace('\n','').replace('\t',''))]

def fasta(fname):
    fasta_sequences = SeqIO.parse(open(fname),'fasta')
    return [(str(f.id), str(f.seq).lower()) for f in fasta_sequences]

def genbank(fname):
    recs = SeqIO.parse(open(fname), 'genbank')
    return [(rec.name, str(rec.seq).lower()) for rec in recs]

PARSERS = {'txt':txt, 'fasta':fasta, 'genbank':genbank}    

def parse(fname):
    return PARSERS[extns.EXT_DCT[fname.split('.')[-1]]](fname)


