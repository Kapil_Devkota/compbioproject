comp bio project
	
here's how to get the web app running locally:

 1. go to `/compbioproject/web` in terminal
 2. enter this command: `source venv/bin/activate`
 3. enter this command: `pip install -r requirements.txt`
 4. enter this command: `export FLASK_APP=web_pal.py`
 5. run app with this command: `flask run`
   - at this point, navigate to http://127.0.0.1:5000/ in web browser
